$(function() {

    Morris.Area({
        element: 'morris-area-chart',
        data: [{
            period: '2015 Q1',
            _048_ua: 26,
            odpublic_net: 25,
            timer_odessa_net: 26
        }, {
            period: '2015 Q2',
            _048_ua: 27,
            odpublic_net: 22,
            timer_odessa_net: 24
        }, {
            period: '2015 Q3',
            _048_ua: 49,
            odpublic_net: 19,
            timer_odessa_net: 25
        }, {
            period: '2015 Q4',
            _048_ua: 37,
            odpublic_net: 35,
            timer_odessa_net: 56
        }, {
            period: '2016 Q1',
            _048_ua: 68,
            odpublic_net: 19,
            timer_odessa_net: 22
        }, {
            period: '2016 Q2',
            _048_ua: 56,
            odpublic_net: 42,
            timer_odessa_net: 18
        }, {
            period: '2016 Q3',
            _048_ua: 48,
            odpublic_net: 37,
            timer_odessa_net: 15
        }, {
            period: '2016 Q4',
            _048_ua: 30,
            odpublic_net: 59,
            timer_odessa_net: 16
        }, {
            period: '2017 Q1',
            _048_ua: 43,
            odpublic_net: 38,
            timer_odessa_net: 29
        }, {
            period: '2017 Q2',
            _048_ua: 28,
            odpublic_net: 32,
            timer_odessa_net: 20
        }],
        xkey: 'period',
        ykeys: ['_048_ua', 'odpublic_net', 'timer_odessa_net'],
        labels: ['048.ua', 'odpublic.net', 'timer-odessa.net'],
        pointSize: 2,
        hideHover: 'auto',
        resize: true
    });

    Morris.Donut({
        element: 'morris-donut-chart',
        data: [{
            label: "Download Sales",
            value: 12
        }, {
            label: "In-Store Sales",
            value: 30
        }, {
            label: "Mail-Order Sales",
            value: 20
        }],
        resize: true
    });

    Morris.Bar({
        element: 'morris-bar-chart',
        data: [{
            y: '2006',
            a: 100,
            b: 90
        }, {
            y: '2007',
            a: 75,
            b: 65
        }, {
            y: '2008',
            a: 50,
            b: 40
        }, {
            y: '2009',
            a: 75,
            b: 65
        }, {
            y: '2010',
            a: 50,
            b: 40
        }, {
            y: '2011',
            a: 75,
            b: 65
        }, {
            y: '2012',
            a: 100,
            b: 90
        }],
        xkey: 'y',
        ykeys: ['a', 'b'],
        labels: ['Series A', 'Series B'],
        hideHover: 'auto',
        resize: true
    });
    
});
