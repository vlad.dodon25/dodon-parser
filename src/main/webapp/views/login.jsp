<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
<head>
    <link href="../public/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="../public/css/sb-admin-2.min.css" rel="stylesheet">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Авторизація</title>

</head>

<body>

<header class="intro-header" style="background-color: white">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="site-heading">
                    <a href="http://www.onaft.edu.ua" class="onaft">
                        <img src="../public/img/gerb.png" width="100" height="88" align="left" hspace="20" name="Герб" style="border: 0px solid red;">
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="container">

        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Будь ласка, введіть данні для авторизіці</h3>
                </div>
                <div class="panel-body">
                    <form role="form" method="post">
                        <fieldset>
                            <div class="form-group">
                                <label for="username">Логін</label>
                                <input  id="username" type="text" class="form-control" placeholder="Логін" name="username" autofocus="">
                            </div>
                            <div class="form-group">
                                <label for="password">Пароль</label>
                                <input id="password" type="password" class="form-control" placeholder="Пароль" name="password"  value="">
                            </div>
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                            <!-- Change this to a button or input when using this as a form -->
                            <input type="submit" class="btn btn-lg btn-success btn-block" value="Увійти"/>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="../public/js/jquery.js"></script>
<script src="../public/js/bootstrap.min.js"></script>

</body>
</html>