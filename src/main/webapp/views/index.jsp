<%@ page import="java.util.List" %>
<%@ page import="com.onapt.project.entity.Article" %>
<%@ page import="com.onapt.project.dao.interf.PhotoDAO" %>
<%@ page import="com.onapt.project.dao.interf.SiteDAO" %>
<%@ page import="com.onapt.project.entity.Site" %>
<%@ page import="com.onapt.project.entity.Photo" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Новини</title>
    <link href="../public/css/bootstrap.min.css" rel="stylesheet">
    <link href="../public/css/blog-home.css" rel="stylesheet">
</head>
<body>
<!-- Navigation -->
<%@include file="../public/jspf/navbar.jspf" %>
<!-- Page Content -->
<div class="container">
    <div class="row">

        <div class="col-md-12">
            <h1 class="page-header">Знайдені новини</h1>
        </div>
        <div class="col-md-9">

            <% PhotoDAO photoDAO = (PhotoDAO) request.getAttribute("photoDAO");
                SiteDAO siteDAO = (SiteDAO) request.getAttribute("siteDAO");
                List<Article> articles = (List<Article>) request.getAttribute("articles");
                for (Article article : articles) {
            %>
            <div class="item-news">
                <div class="row">
                    <div class="col-lg-12 header-news">
                        <h2>
                            <a href="/news/<%=article.getArticleId()%>"><%=article.getName()%></a>
                        </h2>
                        <p class="lead">
                            Знайдено на: <a href="<%=article.getUrl()%>"><%=siteDAO.getSiteById(article.getSiteId()).getName()%></a>
                        </p>
                        <p>
                            <span class="glyphicon glyphicon-time"></span>
                            <span> Опубліковано: </span>
                            <span><%=article.getPublishingDate()%></span>
                        </p>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <% List<Photo> photos = photoDAO.getAllPhotosBy(article.getArticleId());
                                int n = 0;
                                for (Photo photo : photos) {
                                    n++;
                                }
                            %>
                            <div id="carousel-<%=article.getArticleId()%>" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="#carousel-1" data-slide-to="0" class="active"></li>
                                    <%  for (int i = 1; i < n; i++) { %>
                                    <li data-target="#carousel-1" data-slide-to="<%=i%>" class=""></li>

                                    <%}%>
                                </ol>
                                <div class="carousel-inner">
                                    <% for (Photo photo : photos) { %>
                                    <div class="item">
                                        <img class="slide-image" src="<%=photo.getUrl()%>" alt="">
                                    </div>
                                    <%}%>
                                </div>
                                <a class="left carousel-control" href="#carousel-1" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                </a>
                                <a class="right carousel-control" href="#carousel-1" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <p><%=article.getAnnotation()%></p>
                            <a class="btn btn-primary" href="/news/<%=article.getArticleId()%>">Читати повністю<span
                                    class="glyphicon glyphicon-chevron-right"></span></a>
                        </div>
                    </div>
                    <hr>
                </div>
            </div>

            <%}%>

            <div class="row">

                <div class="col-lg-12 text-center">
                    <a class="btn btn-primary" href="#">Більше новин...</a>
                </div>
            </div>
        </div>


        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title text-center">Пошук</h1>
                </div>
                <div class="panel-body">
                    <!--<label for="inputNews" class="sr-only">Название новости</label>-->
                    <input type="text" id="inputNews" class="form-control" placeholder="Назва новини" required=""
                           autofocus="">
                    <p></p>
                    <h5 class="text-center">Діапазон часу</h5>

                    <input type="date" class="form-control">
                    <p></p>
                    <input type="date" class="form-control">

                    <p></p>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h1 class="panel-title">Сайти джерела</h1>
                        </div>
                        <div class="panel-body">
                            <div class="site">
                                <%  List<Site> sites = siteDAO.getAllSites();
                                for (Site site : sites) {
                                %>
                                <input type="checkbox" checked> <%=site.getName()%>
                                <br>

                                <%}%>
                            </div>
                        </div>
                    </div>
                    <a class="btn btn-primary form-control" href="#">Очистити</a>
                    <h5 class="text-center">Кому відправити?</h5>
                    <input type="text" id="inputEmail" class="form-control" placeholder="E-mail" required=""
                           autofocus="">
                    <p></p>
                    <a class="btn btn-primary form-control" href="#">Відправити</a>
                    <p><br></p>
                    <a class="btn btn-primary form-control" href="#">Статистика</a>
                </div>
            </div>


        </div>

    </div>
    <!-- /.row -->
</div>

<!-- Footer -->
<%@include file="../public/jspf/footer.jspf" %>
<!-- /.container -->

<!-- jQuery -->
<script src="../public/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../public/js/bootstrap.min.js"></script>

</body>

</html>
