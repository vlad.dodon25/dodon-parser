<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Налаштування</title>
    <link href="../public/css/bootstrap.min.css" rel="stylesheet">
    <link href="../public/css/blog-home.css" rel="stylesheet">
</head>

<body>
<!-- Navigation -->
<%@include file="../public/jspf/navbar.jspf" %>

<div class="container">
    <div class="row">
        <div class="col-lg-12">

            <div class="panel panel-default">
                <div class="panel-heading" style="font: 22px sans-serif;
    font-weight: 600;">E-mail розсилка
                </div>
                <div class="panel-body">
                    <form role="form">
                        <div class="row">

                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label> E-mail</label>
                                            <input class="form-control" type="text">
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label> Час розсилик</label>
                                            <input class="form-control" type="time">
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label> Інтервал</label>
                                            <select class="form-control">
                                                <option>1 раз день</option>
                                                <option>1 раз в тиждень</option>
                                                <option>1 раз в місяць</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div style="margin-top: 24px">
                                        <div class="col-lg-2">
                                            <input type="submit" name="del" value="Нова розислка"
                                                   class="btn btn-primary form-control">
                                        </div>
                                        <div class="col-lg-2">
                                            <input type="submit" name="del" value="Зберегти розсилку"
                                                   class="btn btn-primary form-control">
                                        </div>
                                        <div class="col-lg-2">
                                            <input type="submit" name="del" value="Видалити розсилку"
                                                   class="btn btn-primary form-control">
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                    <th>№</th>
                                                    <th>E-mail</th>
                                                    <th>Час розсилки</th>
                                                    <th>Інтервал</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>vlad.dodon25@mail.ru</td>
                                                    <td>10:00</td>
                                                    <td>1 раз в день</td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>anton.ivanov@mail.ru</td>
                                                    <td>22:00</td>
                                                    <td>1 раз в тиждень</td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td>victor.plotnik@mail.ru</td>
                                                    <td>14:00</td>
                                                    <td>1 раз в місяць</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>

                                    <div class="col-lg-3">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" style="height: 28px;
    padding-top: 6px;">
                                                <h1 class="panel-title">
                                                    <label>Перелік сайтів-джерел</label>
                                                </h1>
                                            </div>
                                            <div class="panel-body">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" checked> Сайт 1
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" checked> Сайт 2
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" checked> Сайт 3
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-3">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" style="height: 28px;
    padding-top: 6px;">
                                                <h1 class="panel-title">
                                                    <label>Перелік словників</label>
                                                </h1>
                                            </div>
                                            <div class="panel-body">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" checked> Словарь 1
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" checked> Словарь 2
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" checked> Словарь 3
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading" style="font: 22px sans-serif;
    font-weight: 600;">
                    База слів
                </div>
                <div class="panel-body">
                    <form role="form">
                        <div class="row">
                            <div class="col-lg-4">

                                <div class="form-group">
                                    <label>Словник</label>
                                    <select multiple="" class="form-control" style="height: 200px">
                                        <option>ОНАПТ</option>
                                        <option>Егоров</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Назва словника</label>
                                    <input class="form-control">
                                </div>
                            </div>

                            <div class="col-lg-8">
                                <div class="form-group">
                                    <label>Перелік слів</label>
                                    <textarea class="form-control" rows="10" style="height: 200px">ОНАПТ;ONAFT;одесская национальная академия пищевых технологий;одеська національна академія харчових технологій;odessa national academy of food technologies;</textarea>
                                </div>

                                <div class="row" style="margin-top: 39px">

                                    <div class="col-lg-4">
                                        <input type="submit" name="del" value="Новий словник"
                                               class="btn btn-primary form-control">
                                    </div>
                                    <div class="col-lg-4">
                                        <input type="submit" name="del" value="Зберегти словник"
                                               class="btn btn-primary form-control">
                                    </div>

                                    <div class="col-lg-4">
                                        <input type="submit" name="del" value="Видалити словник"
                                               class="btn btn-primary form-control">
                                    </div>

                                </div>

                            </div>


                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>


<!-- Footer -->
<%@include file="../public/jspf/footer.jspf" %>

<!-- /.container -->

<!-- jQuery -->
<script src="../public/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../public/js/bootstrap.min.js"></script>

</body>
</html>