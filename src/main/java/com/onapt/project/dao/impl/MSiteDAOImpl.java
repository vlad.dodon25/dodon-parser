package com.onapt.project.dao.impl;

import com.onapt.project.dao.interf.MSiteDAO;
import com.onapt.project.entity.MSite;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Transactional
@Repository
public class MSiteDAOImpl implements MSiteDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    @Override
    public List<MSite> getAllMSite() {
        String hql = "FROM MSite as MS ORDER BY MS.msId";
        return (List<MSite>) entityManager.createQuery(hql).getResultList();
    }

    @Override
    public List<MSite> getAllMSiteBy(Integer mailingId) {
        String hql = "FROM MSite as MS WHERE MS.mailingId = ? ORDER BY MS.msId";
        return (List<MSite>) entityManager.createQuery(hql)
                .setParameter(1,mailingId).getResultList();
    }

    @Override
    public MSite getMSiteById(Integer mSiteId) {
        return entityManager.find(MSite.class, mSiteId);
    }

    @Override
    public void addMSite(MSite mSite) {
        entityManager.persist(mSite);
    }

    @Override
    public void updateMSite(MSite mSite) {
        MSite mSite1 = getMSiteById(mSite.getMsId());
        mSite1.setMailing(mSite.getMailing());
        mSite1.setSite(mSite.getSite());
        mSite1.setLastSendArticleId(mSite.getLastSendArticleId());
        entityManager.flush();
    }

    @Override
    public void deleteMSite(Integer mSiteId) {
        entityManager.remove(getMSiteById(mSiteId));
    }

    @Override
    public boolean mSiteExists(MSite mSite) {
        String hql = "FROM MSite as MS WHERE MS.mailing_id = ? and MS.site_id = ?";
        int count = entityManager.createQuery(hql).setParameter(1,mSite.getMailing().getMailingId())
                .setParameter(2, mSite.getSite().getSiteId()).getResultList().size();
        return count > 0;
    }
}
