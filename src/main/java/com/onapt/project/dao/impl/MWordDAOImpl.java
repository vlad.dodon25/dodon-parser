package com.onapt.project.dao.impl;

import com.onapt.project.dao.interf.MWordDAO;
import com.onapt.project.entity.MWord;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Transactional
@Repository
public class MWordDAOImpl implements MWordDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    @Override
    public List<MWord> getAllMWord() {
        String hql = "FROM MWord as MW ORDER BY MW.mwId";
        return (List<MWord>) entityManager.createQuery(hql).getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<MWord> getAllMWord(Integer mailingId){
        String hql = "FROM MWord as MW WHERE MW.mailingId = ? ORDER BY MW.mwId";
        return (List<MWord>) entityManager.createQuery(hql)
                .setParameter(1,mailingId).getResultList();
    }

    @Override
    public MWord getMWordById(Integer mWordId) {
        return entityManager.find(MWord.class, mWordId);
    }

    @Override
    public void addMWord(MWord mWord) {
        entityManager.persist(mWord);
    }

    @Override
    public void updateMWord(MWord mWord) {
        MWord mWord1 = getMWordById(mWord.getMwId());
        mWord1.setMailing(mWord.getMailing());
        mWord1.setWord(mWord.getWord());
        entityManager.flush();
    }

    @Override
    public void deleteMWord(Integer mWordId) {
        entityManager.remove(getMWordById(mWordId));
    }

    @Override
    public boolean mWordExists(MWord mWord) {
        String hql = "FROM MWord as MW WHERE MW.mailing_id = ? and MW.wordId = ?";
        int count = entityManager.createQuery(hql).setParameter(1,mWord.getMailing().getMailingId())
                .setParameter(2, mWord.getWord().getWordId()).getResultList().size();
        return count > 0;
    }
}
