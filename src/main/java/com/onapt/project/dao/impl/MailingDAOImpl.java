package com.onapt.project.dao.impl;

import com.onapt.project.dao.interf.MailingDAO;
import com.onapt.project.entity.Mailing;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Transactional
@Repository
public class MailingDAOImpl implements MailingDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    @Override
    public List<Mailing> getAllMailings() {
        String hql = "FROM Mailing as M ORDER BY M.mailingId";
        return (List<Mailing>) entityManager.createQuery(hql).getResultList();
    }

    @Override
    public Mailing getMailingById(Integer mailingId) {
        return entityManager.find(Mailing.class, mailingId);
    }

    @Override
    public void addMailing(Mailing mailing) {
        entityManager.persist(mailing);
    }

    @Override
    public void updateMailing(Mailing mailing) {
        Mailing mailing1 = getMailingById(mailing.getMailingId());
        mailing1.setIssending(mailing.getIssending());
        mailing1.setTime(mailing.getTime());
        mailing1.settInterval(mailing.gettInterval());
        entityManager.flush();
    }

    @Override
    public void deleteMailing(Integer mailingId) {
        entityManager.remove(getMailingById(mailingId));
    }

    @Override
    public boolean articleExists(Mailing mailing) {
        String hql = "FROM Mailing as M WHERE M.email = ? and M.issending = ? and M.time = ? and M.tiId = ?";
        int count = entityManager.createQuery(hql).setParameter(1,mailing.getEmail())
                .setParameter(2, mailing.getIssending()).setParameter(3, mailing.getTime())
                .setParameter(4,mailing.gettInterval().getTiId()).getResultList().size();
        return count > 0;
    }
}
