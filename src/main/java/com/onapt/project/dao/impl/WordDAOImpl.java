package com.onapt.project.dao.impl;

import com.onapt.project.dao.interf.WordDAO;
import com.onapt.project.entity.Word;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Transactional
@Repository
public class WordDAOImpl implements WordDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    @Override
    public List<Word> getAllWord() {
        String hql = "FROM Word as W ORDER BY W.wordId";
        return (List<Word>) entityManager.createQuery(hql).getResultList();
    }

    @Override
    public Word getWordById(Integer wordId) {
        return entityManager.find(Word.class,wordId);
    }

    @Override
    public void addWord(Word word) {
        word.setWords(word.getWords().toLowerCase());
        entityManager.persist(word);

    }

    @Override
    public void updateWord(Word word) {
        Word word1 = getWordById((word.getWordId()));
        word1.setName(word.getName());
        word.setWords(word1.getWords().toLowerCase());
        entityManager.flush();
    }

    @Override
    public void deleteWord(Integer wordId) {
        entityManager.remove(getWordById(wordId));
    }

    @Override
    public boolean wordExists(Word word) {
        String hql = "FROM Word as W WHERE W.name = ? and W.words = ?";
        int count = entityManager.createQuery(hql).setParameter(1, word.getName())
                .setParameter(2, word.getWords()).getResultList().size();
        return count > 0;
    }
}
