package com.onapt.project.dao.impl;


import com.onapt.project.dao.interf.UserDAO;
import com.onapt.project.entity.User;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Transactional
@Repository
public class UserDAOImpl implements UserDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    @Override
    public List<User> getAllUsers() {
        String hql = "FROM User as U ORDER BY U.userId";
        return (List<User>) entityManager.createQuery(hql).getResultList();
    }

    @Override
    public User getUserById(Integer userId) {
        return entityManager.find(User.class, userId);
    }

    @Override
    public void addUser(User user) {
        entityManager.persist(user);
    }

    @Override
    public void updateSentArticle(User user) {
        User user1 = getUserById(user.getUserId());
        user1.setFirstName(user.getFirstName());
        user1.setLastName(user.getLastName());
        user1.setLogin(user.getLogin());
        user1.setPassword(user.getPassword());
        user1.setEmail(user.getEmail());
        user1.setPhone(user.getPhone());
        entityManager.flush();
    }

    @Override
    public void deleteUser(Integer userId) {
        entityManager.remove(getUserById(userId));
    }

    @Override
    public boolean userExists(User user) {
        String hql = "FROM User as U WHERE U.firstName = ? and U.lastName = ? and U.login = ? " +
                "and U.password = ? and U.email = ? and U.phone = ?";
        int count = entityManager.createQuery(hql).setParameter(1,user.getFirstName())
                .setParameter(2, user.getLastName()).setParameter(3, user.getLogin())
                .setParameter(4, user.getPassword()).setParameter(5, user.getEmail())
                .setParameter(6, user.getPhone()).getResultList().size();
        return count > 0;
    }
}
