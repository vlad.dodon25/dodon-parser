package com.onapt.project.dao.impl;


import com.onapt.project.dao.interf.ArticleDAO;
import com.onapt.project.entity.Article;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.Timestamp;
import java.util.List;
import java.util.NoSuchElementException;

@Transactional
@Repository
public class ArticleDAOImpl implements ArticleDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<Article> getAllArticles() {
		String hql = "FROM Article as A ORDER BY A.publishingDate DESC";
		return (List<Article>) entityManager.createQuery(hql).getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Article> getAllArticlesBy(Integer siteId) {
		String hql = "FROM Article as A WHERE A.siteId = ? ORDER BY A.publishingDate DESC";
		return (List<Article>) entityManager.createQuery(hql).setParameter(1,siteId).getResultList();
	}

	@Override
	public List<Article> getAllArticlesBy(Integer siteId, Timestamp fromDate) {
		String hql = "FROM Article as A WHERE A.siteId = ?  and A.publishingDate > ? ORDER BY A.publishingDate DESC";
		return (List<Article>) entityManager.createQuery(hql).setParameter(1,siteId)
				.setParameter(2,fromDate).getResultList();
	}

	@SuppressWarnings("unchecked")
    @Override
    public Article getArticleBy(String name, String url) {
        String hql = "FROM Article as A WHERE A.name = ? and A.url = ?";
        try {
            return (Article) entityManager.createQuery(hql)
                    .setParameter(1, name)
                    .setParameter(2, url).getResultList().iterator().next();
        }
        catch (NoSuchElementException e) {
            return null;
        }
    }


	@Override
	public Article getArticleBy(Integer articleId) {
		return entityManager.find(Article.class, articleId);
	}

	@Override
	public void addArticle(Article article) {
		entityManager.persist(article);
	}

	@Override
	public void updateArticle(Article article) {
		Article article1 = getArticleBy(article.getArticleId());
		article1.setFindingDate(article.getFindingDate());
		article1.setPublishingDate(article.getPublishingDate());
		article1.setUrl(article.getUrl());
		article1.setName(article.getName());
		article1.setAnnotation(article.getAnnotation());
		article1.setContent(article.getContent());
		article1.setSite(article.getSite());
		entityManager.flush();
	}

	@Override
	public void deleteArticle(Integer articleId) {
		entityManager.remove(getArticleBy(articleId));
	}

	@Override
	public boolean articleExists(Article article) {
		String hql = "FROM Article as A WHERE  A.name = ? and A.url = ?";
		int count = entityManager.createQuery(hql)
				.setParameter(1,article.getName())
				.setParameter(2,article.getUrl())
				.getResultList().size();
		return count > 0;
	}

}
