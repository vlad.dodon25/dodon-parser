package com.onapt.project.dao.impl;

import com.onapt.project.dao.interf.MailingListDAO;
import com.onapt.project.entity.MailingList;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.Timestamp;
import java.util.List;
import java.util.NoSuchElementException;

@Transactional
@Repository
public class MailingListDAOImpl implements MailingListDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    @Override
    public List<MailingList> getAllMailingLists() {
        String hql = "FROM MailingList as ML ORDER BY ML.mlId";
        return (List<MailingList>) entityManager.createQuery(hql).getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public  List<MailingList> getAllMailingListBy(Integer mailingId) {
        String hql = "FROM MailingList as ML WHERE ML.mailingId = ? ORDER BY ML.datatime DESC";
        return (List<MailingList>) entityManager.createQuery(hql)
                .setParameter(1,mailingId).getResultList();
    }

    @Override
    public MailingList getMailingListById(Integer mailingListId) {
        return entityManager.find(MailingList.class, mailingListId);
    }

    @SuppressWarnings("unchecked")
    @Override
    public MailingList getMailingListBy(Timestamp dateTime) {
        String hql = "FROM MailingList as ML WHERE ML.datatime = ? ORDER BY ML.datatime DESC";
        MailingList mailingList;
        try {
            mailingList = (MailingList) entityManager.createQuery(hql).setParameter(1,dateTime)
                    .getResultList().iterator().next();
        } catch (NoSuchElementException e) {
            mailingList = null;
        }
        return mailingList;
    }

    @Override
    public void addMailingList(MailingList mailingList) {
        entityManager.persist(mailingList);
    }

    @Override
    public void updateMailingList(MailingList mailingList) {
        MailingList mailingList1 = getMailingListById(mailingList.getMlId());
        mailingList1.setMailing(mailingList.getMailing());
        mailingList1.setDatatime(mailingList.getDatatime());
        entityManager.flush();
    }

    @Override
    public void deleteMailingList(Integer mailingListId) {
        entityManager.remove(getMailingListById(mailingListId));
    }

    @Override
    public boolean articleExists(MailingList mailingList) {
        String hql = "FROM MailingList as ML WHERE ML.mailing_id = ? and ML.datatime = ?";
        int count = entityManager.createQuery(hql).setParameter(1,mailingList.getMailing().getMailingId())
                .setParameter(2, mailingList.getDatatime()).getResultList().size();
        return count > 0;
    }
}
