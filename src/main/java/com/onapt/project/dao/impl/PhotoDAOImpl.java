package com.onapt.project.dao.impl;

import com.onapt.project.dao.interf.PhotoDAO;
import com.onapt.project.entity.Photo;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Transactional
@Repository
public class PhotoDAOImpl implements PhotoDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    @Override
    public List<Photo> getAllPhotos() {
        String hql = "FROM Photo as P ORDER BY P.photoId";
        return (List<Photo>) entityManager.createQuery(hql).getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Photo> getAllPhotosBy(Integer articleId) {
        String hql = "FROM Photo as P WHERE P.articleId = ? ORDER BY P.photoId";
        return (List<Photo>) entityManager.createQuery(hql)
                .setParameter(1,articleId).getResultList();
    }

    @Override
    public Photo getPhotoById(Integer photoId) {
        return entityManager.find(Photo.class, photoId);
    }



    @Override
    public void addPhoto(Photo photo) {
        entityManager.persist(photo);
    }

    @Override
    public void updatePhoto(Photo photo) {
        Photo photo1 = getPhotoById(photo.getPhotoId());
        photo1.setArticle(photo.getArticle());
        photo1.setUrl(photo.getUrl());
        entityManager.flush();
    }

    @Override
    public void deletePhoto(Integer photoId) {
        entityManager.remove(getPhotoById(photoId));
    }

    @Override
    public boolean photoExists(Photo photo) {
        String hql = "FROM Photo as P WHERE P.url = ? and P.articleId = ?";
        int count = entityManager.createQuery(hql).setParameter(1,photo.getUrl())
                .setParameter(2, photo.getArticle().getArticleId()).getResultList().size();
        return count > 0;
    }
}
