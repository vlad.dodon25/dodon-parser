package com.onapt.project.dao.impl;

import com.onapt.project.dao.interf.SendArticleDAO;
import com.onapt.project.entity.SendArticle;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Transactional
@Repository
public class SendArticleDAOImpl implements SendArticleDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    @Override
    public List<SendArticle> getAllSendArticles() {
        String hql = "FROM SendArticle as SA ORDER BY SA.saId";
        return (List<SendArticle>) entityManager.createQuery(hql).getResultList();
    }

    @Override
    public SendArticle getSendArticleById(Integer sendArticleId) {
        return entityManager.find(SendArticle.class, sendArticleId);
    }

    @Override
    public void addSendArticle(SendArticle sendArticle) {
        entityManager.persist(sendArticle);
    }

    @Override
    public void updateSendArticle(SendArticle sendArticle) {
        SendArticle sendArticle1 = getSendArticleById(sendArticle.getSaId());
        sendArticle1.setArticle(sendArticle.getArticle());
        sendArticle1.setMailingList(sendArticle.getMailingList());
        entityManager.flush();
    }

    @Override
    public void deleteSendArticle(Integer sendArticleId) {
        entityManager.remove(getSendArticleById(sendArticleId));
    }

    @Override
    public boolean sendArticleExists(SendArticle sendArticle) {
        String hql = "FROM SendArticle as SA WHERE SA.articleId = ? and SA.mlId = ?";
        int count = entityManager.createQuery(hql).setParameter(1, sendArticle.getArticle().getArticleId())
                .setParameter(2, sendArticle.getMailingList().getMlId()).getResultList().size();
        return count > 0;
    }
}
