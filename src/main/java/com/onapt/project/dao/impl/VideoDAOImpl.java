package com.onapt.project.dao.impl;

import com.onapt.project.dao.interf.VideoDAO;
import com.onapt.project.entity.Video;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Transactional
@Repository
public class VideoDAOImpl implements VideoDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    @Override
    public List<Video> getAllVideos() {
        String hql = "FROM Video as V ORDER BY V.videoId";
        return (List<Video>) entityManager.createQuery(hql).getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Video> getAllVideosBy(Integer articleId) {
        String hql = "FROM Video as V WHERE V.articleId = ? ORDER BY V.videoId";
        return (List<Video>) entityManager.createQuery(hql)
                .setParameter(1,articleId).getResultList();
    }

    @Override
    public Video getVideoById(Integer videoId) {
        return entityManager.find(Video.class, videoId);
    }

    @Override
    public void addVideo(Video video) {
        entityManager.persist(video);
    }

    @Override
    public void updateVideo(Video video) {
        Video video1 = getVideoById(video.getVideoId());
        video1.setArticle(video.getArticle());
        video1.setUrl(video.getUrl());
        entityManager.flush();
    }

    @Override
    public void deleteVideo(Integer videoId) {
        entityManager.remove(getVideoById(videoId));
    }

    @Override
    public boolean videoExists(Video video) {
        String hql = "FROM Video as V WHERE V.url = ? and V.articleId = ?";
        int count = entityManager.createQuery(hql).setParameter(1,video.getUrl())
                .setParameter(2, video.getArticle().getArticleId()).getResultList().size();
        return count > 0;
    }
}
