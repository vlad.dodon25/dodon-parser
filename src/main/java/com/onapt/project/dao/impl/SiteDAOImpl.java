package com.onapt.project.dao.impl;


import com.onapt.project.dao.interf.SiteDAO;
import com.onapt.project.entity.Site;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Transactional
@Repository
public class SiteDAOImpl implements SiteDAO {


    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    @Override
    public List<Site> getAllSites() {
        String hql = "FROM Site as S ORDER BY S.siteId";
        return (List<Site>) entityManager.createQuery(hql).getResultList();
    }

    @Override
    public Site getSiteById(Integer siteId) {
        return entityManager.find(Site.class, siteId);
    }

    @Override
    public void addSite(Site site) {
        entityManager.persist(site);
    }

    @Override
    public void updateSite(Site site) {
        Site site1 = getSiteById(site.getSiteId());
        site1.setName(site.getName());
        site1.setUrl(site.getUrl());
        site1.setPagination(site.getPagination());
        site1.setLastArticleUrl(site.getLastArticleUrl());
        entityManager.flush();
    }

    @Override
    public void deleteSite(Integer siteId) {
        entityManager.remove(getSiteById((siteId)));
    }

    @Override
    public boolean siteExists(Site site) {
        String hql = "FROM Site as S WHERE S.name = ? and S.url = ? and S.pagination";
        int count = entityManager.createQuery(hql).setParameter(1, site.getName())
                .setParameter(2, site.getUrl()).setParameter(3, site.getPagination())
                .getResultList().size();
        return count > 0;
    }

}
