package com.onapt.project.dao.impl;

import com.onapt.project.dao.interf.TIntervalDAO;
import com.onapt.project.entity.TInterval;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Transactional
@Repository
public class TIntervalDAOImpl implements TIntervalDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    @Override
    public List<TInterval> getAllTInterval() {
        String hql = "FROM TInterval as TI ORDER BY TI.tiId";
        return
                (List<TInterval>)
                        entityManager.
                                createQuery(hql).
                                getResultList();
    }

    @Override
    public TInterval getTIntervalById(Integer tIntervalId) {
        return entityManager.find(TInterval.class, tIntervalId);
    }

    @Override
    public void addTInterval(TInterval tInterval) {
        entityManager.persist(tInterval);
    }

    @Override
    public void updateTInterval(TInterval tInterval) {
        TInterval tInterval1 = getTIntervalById(tInterval.getTiId());
        tInterval1.setName(tInterval.getName());
        tInterval1.setInterval(tInterval1.getInterval());
        entityManager.flush();
    }

    @Override
    public void deleteTInterval(Integer tIntervalId) {
        entityManager.remove(tIntervalId);
    }

    @Override
    public boolean tIntervalExists(TInterval tInterval) {
        String hql = "FROM TInterval as TI WHERE TI.name = ? and TI.interval = ?";
        int count = entityManager.createQuery(hql).setParameter(1, tInterval.getName())
                .setParameter(2, tInterval.getInterval()).getResultList().size();
        return count > 0;
    }
}
