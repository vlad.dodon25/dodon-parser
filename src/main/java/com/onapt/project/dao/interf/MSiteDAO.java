package com.onapt.project.dao.interf;

import com.onapt.project.entity.MSite;

import java.util.List;

public interface MSiteDAO {

    List<MSite> getAllMSite();

    List<MSite> getAllMSiteBy(Integer mailingId);

    MSite getMSiteById(Integer mSiteId);

    void addMSite(MSite mSite);

    void updateMSite(MSite mSite);

    void deleteMSite(Integer mSiteId);

    boolean mSiteExists(MSite mSite);

}
