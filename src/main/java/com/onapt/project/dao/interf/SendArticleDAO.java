package com.onapt.project.dao.interf;


import com.onapt.project.entity.SendArticle;

import java.util.List;

public interface SendArticleDAO {

    List<SendArticle> getAllSendArticles();

    SendArticle getSendArticleById(Integer sendArticleId);

    void addSendArticle(SendArticle sendArticle);

    void updateSendArticle(SendArticle sendArticle);

    void deleteSendArticle(Integer sendArticleId);

    boolean sendArticleExists(SendArticle sendArticle);

}
