package com.onapt.project.dao.interf;

import com.onapt.project.entity.Word;

import java.util.List;

public interface WordDAO {

    List<Word> getAllWord();

    Word getWordById(Integer wordId);

    void addWord(Word word);

    void updateWord(Word word);

    void deleteWord(Integer wordId);

    boolean wordExists(Word word);

}
