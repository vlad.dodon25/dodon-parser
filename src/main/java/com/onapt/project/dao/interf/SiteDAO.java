package com.onapt.project.dao.interf;


import com.onapt.project.entity.Site;

import java.util.List;

public interface SiteDAO {

    List<Site> getAllSites();

    Site getSiteById(Integer siteId);

    void addSite(Site site);

    void updateSite(Site site);

    void deleteSite(Integer siteId);

    boolean siteExists(Site site);

}
