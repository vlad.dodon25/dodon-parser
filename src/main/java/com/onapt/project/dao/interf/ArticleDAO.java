package com.onapt.project.dao.interf;

import com.onapt.project.entity.Article;
import com.onapt.project.entity.MSite;

import java.sql.Timestamp;
import java.util.List;

public interface ArticleDAO {

    List<Article> getAllArticles();

    List<Article> getAllArticlesBy(Integer siteId);

    List<Article> getAllArticlesBy(Integer siteId, Timestamp fromDate);

    Article getArticleBy(Integer articleId);

    Article getArticleBy(String name, String url);

    void addArticle(Article article);

    void updateArticle(Article article);

    void deleteArticle(Integer articleId);

    boolean articleExists(Article article);

}
 