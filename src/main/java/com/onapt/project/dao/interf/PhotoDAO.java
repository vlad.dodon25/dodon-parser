package com.onapt.project.dao.interf;

import com.onapt.project.entity.Photo;

import java.util.List;

public interface PhotoDAO {

    List<Photo> getAllPhotos();

    List<Photo>  getAllPhotosBy(Integer articleId);

    Photo getPhotoById(Integer photoId);

    void addPhoto(Photo photo);

    void updatePhoto(Photo photo);

    void deletePhoto(Integer photoId);

    boolean photoExists(Photo photo);

}
