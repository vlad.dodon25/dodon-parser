package com.onapt.project.dao.interf;



import com.onapt.project.entity.User;

import java.util.List;

public interface UserDAO {

    List<User> getAllUsers();

    User getUserById(Integer userId);

    void addUser(User user);

    void updateSentArticle(User user);

    void deleteUser(Integer userId);

    boolean userExists(User user);

}
