package com.onapt.project.dao.interf;

import com.onapt.project.entity.Photo;
import com.onapt.project.entity.Video;

import java.util.List;

public interface VideoDAO {

    List<Video> getAllVideos();

    List<Video>  getAllVideosBy(Integer articleId);

    Video getVideoById(Integer videoId);

    void addVideo(Video video);

    void updateVideo(Video video);

    void deleteVideo(Integer videoId);

    boolean videoExists(Video video);

}
