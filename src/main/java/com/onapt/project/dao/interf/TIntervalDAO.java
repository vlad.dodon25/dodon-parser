package com.onapt.project.dao.interf;

import com.onapt.project.entity.TInterval;

import java.util.List;

public interface TIntervalDAO {

    List<TInterval> getAllTInterval();

    TInterval getTIntervalById(Integer tIntervalId);

    void addTInterval(TInterval tInterval);

    void updateTInterval(TInterval tInterval);

    void deleteTInterval(Integer tIntervalId);

    boolean tIntervalExists(TInterval tInterval);

}
