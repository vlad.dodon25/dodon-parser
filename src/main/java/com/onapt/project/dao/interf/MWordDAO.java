package com.onapt.project.dao.interf;

import com.onapt.project.entity.MWord;

import java.util.List;

public interface MWordDAO {

    List<MWord> getAllMWord();

    List<MWord> getAllMWord(Integer mailingId);

    MWord getMWordById(Integer mWordId);

    void addMWord(MWord mWord);

    void updateMWord(MWord mWord);

    void deleteMWord(Integer mWordId);

    boolean mWordExists(MWord mWord);

}
