package com.onapt.project.dao.interf;

import com.onapt.project.entity.MailingList;

import java.sql.Timestamp;
import java.util.List;

public interface MailingListDAO {

    List<MailingList> getAllMailingLists();

    List<MailingList> getAllMailingListBy(Integer mailingId);

    MailingList getMailingListById(Integer mailingListId);

    MailingList getMailingListBy(Timestamp dateTime);

    void addMailingList(MailingList mailingList);

    void updateMailingList(MailingList mailingList);

    void deleteMailingList(Integer mailingListId);

    boolean articleExists(MailingList mailingList);
}
