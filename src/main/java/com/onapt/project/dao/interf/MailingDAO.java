package com.onapt.project.dao.interf;

import com.onapt.project.entity.Mailing;

import java.util.List;

public interface MailingDAO {

    List<Mailing> getAllMailings();

    Mailing getMailingById(Integer mailingId);

    void addMailing(Mailing mailing);

    void updateMailing(Mailing mailing);

    void deleteMailing(Integer mailingId);

    boolean articleExists(Mailing mailing);

}
