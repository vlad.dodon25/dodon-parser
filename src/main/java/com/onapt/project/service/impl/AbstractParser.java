package com.onapt.project.service.impl;

import com.onapt.project.dao.interf.*;
import com.onapt.project.entity.*;
import com.onapt.project.service.interf.Parser;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.StringTokenizer;

public abstract class AbstractParser implements Parser {

    @Autowired
    protected ArticleDAO articleDAO;

    @Autowired
    protected WordDAO wordDAO;

    @Autowired
    protected SiteDAO siteDAO;

    @Autowired
    protected PhotoDAO photoDAO;

    @Autowired
    protected VideoDAO videoDAO;

    protected Site site;

    protected List<Word> words;

    public void initialization(Integer siteId) {
        words = wordDAO.getAllWord();
        site = siteDAO.getSiteById(siteId);
    }

    public boolean isInteresting(Article article) {

        if (article.getContent().isEmpty() || article.getAnnotation().isEmpty() || article.getName().isEmpty())
            return false;
        Integer i = 0;
        for (Word word : words) {
            StringTokenizer st = new StringTokenizer(word.getWords().replace("\n", ""), ";");
            while (st.hasMoreTokens()) {
                String w = st.nextToken();
                if (article.getName().toLowerCase().contains(w) ||
                        article.getContent().toLowerCase().contains(w) ||
                        article.getAnnotation().toLowerCase().contains(w)) {
                    i++;
                }
            }
        }
        if (i > 0) {
            article.setRating(i);
            return !articleDAO.articleExists(article);
        } else {
            return false;
        }
    }

    public void parsVideo(Elements videos, Article article) {
        for (Element element : videos) {
            Video video = new Video();
            video.setArticle(article);
            String url = element.attr("src");
            video.setUrl(url);
            videoDAO.addVideo(video);
        }
    }

    public void parsPhoto(Elements photos, Article article) {
        for (Element element : photos) {
            Photo photo = new Photo();
            photo.setArticle(article);
            String url = element.attr("src");
            if (url.contains(site.getName())) {
                photo.setUrl(url);
            } else {
                photo.setUrl(site.getUrl() + url);
            }
            photoDAO.addPhoto(photo);
        }
    }
}
