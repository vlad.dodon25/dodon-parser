package com.onapt.project.service.impl;

import com.onapt.project.entity.Article;
import com.onapt.project.service.interf.Parser;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;


@EnableScheduling
@Service //parser odpublic.net
public class ParserImpl2 extends AbstractParser implements Parser {

    @Scheduled(fixedRate = 5000)
    @Override
    public void searchNews() {
        initialization(2);
        String lastArticleUrl1 = null;

        int i = 1;
        while (true) {
            Document doc;
            Elements links;
            try {
                doc = Jsoup.connect(site.getPagination() + i).get();
                links = doc.select("ul.news-tab-list li.news-tab-item");
                if (i == 1)
                    lastArticleUrl1 = site.getUrl() + links.first().getElementsByClass("hot-news-title")
                            .first().attr("href");
            } catch (IOException | NullPointerException e) {
                e.printStackTrace();
                if (i != 1) {
                    site.setLastArticleUrl(lastArticleUrl1);
                    siteDAO.updateSite(site);
                }
                return;
            }

            for (Element link : links) {
                Article article = new Article();
                article.setUrl(site.getUrl() + link.getElementsByClass("hot-news-title").attr("href"));
                if (site.getLastArticleUrl() == null || site.getLastArticleUrl().isEmpty() || site.getLastArticleUrl().compareTo(article.getUrl()) != 0) {
                    try {
                        parsNews(article);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    site.setLastArticleUrl(lastArticleUrl1);
                    siteDAO.updateSite(site);
                    return;
                }
            }
            i++;
        }
    }

    private Timestamp parsDate(String date) throws ParseException {
        date = date.replace(",", "");
        Calendar c1 = Calendar.getInstance();
        c1.setTimeInMillis(System.currentTimeMillis());
        String t1;
        DateFormat dateFormat1 = new SimpleDateFormat("yy-MM-dd HH:mm");
        DateFormat dateFormat2 = new SimpleDateFormat("dd.MM.yy HH:mm");
        if (date.toLowerCase().contains("Сегодня".toLowerCase())) {
            t1 = date.toLowerCase().replace("Сегодня".toLowerCase(), "").trim();
            return new Timestamp(dateFormat1.parse(new Date(c1.getTimeInMillis()) + " " + t1).getTime());
        } else {
            String[][] months = {{" июля ", ".07."}, {"авг.", ".08."}, {"сент.", ".09."}, {" окт. ", ".10."}, {" нояб. ", ".11."}, {" дек. ", ".12."},
                    {" янв. ", ".01."}, {" февр. ", ".02."}, {" марта ", ".03."}, {" апр. ", ".04."}, {" мая ", ".05."}, {" июня ", ".06."}};
            for (int i = 0; i < 12; i++) {
                date = date.replace(months[i][0], months[i][1]);
            }
            return new Timestamp(dateFormat2.parse(date).getTime());
        }
    }

    @Override
    public void parsNews(Article article) throws IOException {
        Document doc = Jsoup.connect(article.getUrl()).get();
        Elements date = doc.select("p.date-ico span");
        Elements name = doc.select("h3.news-title");
        Element annotation = doc.select("div.news-text > p").first();
        Elements text = doc.select("div.news-text > p");
        Elements contentPhoto = doc.select("div.news-photo-wrapper img.img-responsive");
        Elements contentVideo = doc.select("div.news-text div.videodetector iframe");

        article.setName(name.text());
        article.setAnnotation(annotation.text());
        article.setContent(text.text().replace(article.getAnnotation() + " ", ""));

        if (isInteresting(article)) {
            article.setFindingDate(new Timestamp(System.currentTimeMillis()));
            try {
                article.setPublishingDate(parsDate(date.text()));
            } catch (ParseException e) {
                e.printStackTrace();
                article.setPublishingDate(article.getFindingDate());
            }
            article.setSite(site);
            articleDAO.addArticle(article);

            parsVideo(contentVideo, article);
            parsPhoto(contentPhoto, article);

        }
    }
}
