//package com.onapt.project.service.impl;
//
//
//import com.onapt.project.dao.interf.ArticleDAO;
//import com.onapt.project.entity.Article;
//import com.onapt.project.service.interf.ArticleService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//
//@Service
//public class ArticleServiceImpl implements ArticleService {
//
//	@Autowired
//	private ArticleDAO articleDAO;
//
//	@Override
//	public Article getArticleById(int articleId) {
//		Article obj = articleDAO.getArticleById(articleId);
//		return obj;
//	}
//
//	@Override
//	public List<Article> getAllArticles(){
//		return articleDAO.getAllArticles();
//	}
//
//	@Override
//	public synchronized boolean addArticle(Article article){
//       if (articleDAO.articleExists(article)) {
//    	   return false;
//       } else {
//    	   articleDAO.addArticle(article);
//    	   return true;
//       }
//	}
//
//	@Override
//	public void updateArticle(Article article) {
//		articleDAO.updateArticle(article);
//	}
//
//	@Override
//	public void deleteArticle(int articleId) {
//		articleDAO.deleteArticle(articleId);
//	}
//
//	//@Override
////	public Article getArticleWithMaxId(int siteId) {
//		//return articleDAO.getArticleWithMaxId(siteId);
//	//}
//
//}
