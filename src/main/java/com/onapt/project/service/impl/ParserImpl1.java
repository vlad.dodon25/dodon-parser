package com.onapt.project.service.impl;

import com.onapt.project.entity.Article;
import com.onapt.project.service.interf.Parser;
import org.jsoup.Jsoup;
import org.jsoup.UnsupportedMimeTypeException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

//@EnableScheduling
@Service    //parser 048.ua
public class ParserImpl1 extends AbstractParser implements Parser {

    //        @Scheduled(fixedRate = 1800000)
    @Override
    public void searchNews() {
        initialization(1);
        String lastArticleUrl1 = null;

        int i = 1;
        while (true) {
            Document doc;
            Elements links;
            try {
                doc = Jsoup.connect(site.getPagination() + i).get();
                links = doc.select("a.namelink");
                if (i == 1)
                    lastArticleUrl1 = site.getUrl() + links.first().attr("href");
            } catch (IOException | NullPointerException e) {
                e.printStackTrace();
                if (i != 1) {
                    site.setLastArticleUrl(lastArticleUrl1);
                    siteDAO.updateSite(site);
                }
                return;
            }

            for (Element link : links) {
                Article article = new Article();
                article.setUrl(site.getUrl() + link.attr("href"));
                if (site.getLastArticleUrl() == null || site.getLastArticleUrl().isEmpty() || site.getLastArticleUrl().compareTo(article.getUrl()) != 0) {
                    try {
                        parsNews(article);
                    } catch (IOException | NullPointerException e) {
                        e.printStackTrace();
                    }
                } else {
                    site.setLastArticleUrl(lastArticleUrl1);
                    siteDAO.updateSite(site);
                    return;
                }
            }
            i++;
        }
    }

    @Override
    public void parsNews(Article article) throws IOException, NullPointerException {
        Document doc = Jsoup.connect(article.getUrl()).get();
        Elements date = doc.select("div.open_news > div.items > div.date > span:not(.specialty)");
        Elements name = doc.select("div.open_news > div.text_header");
        Element annotation = doc.select("div.open_news > div.static p").first();
        Elements content = doc.select("div.open_news > div.static p");
        Elements mainPhoto = doc.select("div.open_news > div.inner-photo-block > div.img-block > img");
        Elements contentPhoto = doc.select("div.open_news > div.static img");
        Elements contentVideo = doc.select("div.open_news > div.static iframe");

        try {
            article.setName(name.text());
            article.setAnnotation(annotation.text());
            article.setContent(content.text().replace(annotation.text(), ""));
        } catch (NullPointerException e) {
            e.printStackTrace();
            System.out.println(article.getUrl());
        }
        if (isInteresting(article)) {
            article.setFindingDate(new Timestamp(System.currentTimeMillis()));
            try {
                article.setPublishingDate(parsDate(date.text()));
            } catch (ParseException e) {
                e.printStackTrace();
                article.setPublishingDate(article.getFindingDate());
            }
            article.setSite(site);
            articleDAO.addArticle(article);

            parsVideo(contentVideo, article);
            parsPhoto(contentPhoto, article);
            parsPhoto(mainPhoto, article);

        }
    }

    public Timestamp parsDate(String date) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("hh:mm dd.MM.yy");
        return new Timestamp(dateFormat.parse(date).getTime());
    }
}
