package com.onapt.project.service.impl;

import com.onapt.project.entity.Article;
import com.onapt.project.service.interf.Parser;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;


@EnableScheduling
@Service //parser dumskaya.net
public class ParserImpl3 extends AbstractParser implements Parser {

//    @Scheduled(fixedRate = 500000)
    @Override
    public void searchNews() {
        initialization(3);
        String lastArticleUrl1 = null;

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis());


        String parination;
        int i = 1;
        while (true) {
            cal.add(Calendar.DATE, -1);
            parination = "/" + cal.get(Calendar.YEAR)  + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.DATE) + "/";
            Document doc;
            Elements links;
            try {
                doc = Jsoup.connect(site.getPagination() + parination).get();
                links = doc.select("table.newstable tr.newstr td:last-child a:first-child");
                if (i == 1)
                    lastArticleUrl1 = site.getUrl() + links.first().getElementsByTag("a").attr("href");
            } catch (IOException | NullPointerException e) {
                e.printStackTrace();
                if (i != 1) {
                    site.setLastArticleUrl(lastArticleUrl1);
                    siteDAO.updateSite(site);
                }
                return;
            }
            for (Element link : links) {
                Article article = new Article();
                article.setUrl(site.getUrl() + link.getElementsByTag("a").attr("href"));
                if (site.getLastArticleUrl() == null || site.getLastArticleUrl().isEmpty() || site.getLastArticleUrl().compareTo(article.getUrl()) != 0)
                    try {
                        parsNews(article);
                    } catch (IOException | NullPointerException e) {
                        e.printStackTrace();
                    }
                else {
                    site.setLastArticleUrl(lastArticleUrl1);
                    siteDAO.updateSite(site);
                    return;
                }
            }
            i++;
        }
    }


    private Timestamp parsDate(String date) throws ParseException {
        date = date.replace(",", "");
        String[][] months = {{" июля ", ".07."}, {"августа", ".08."}, {"сентября", ".09."}, {" октября ", ".10."},
                {" ноября ", ".11."}, {" декабря ", ".12."}, {" января ", ".01."}, {" феврал ", ".02."},
                {" марта ", ".03."}, {" апреля ", ".04."}, {" мая ", ".05."}, {" июня ", ".06."}};
        for (int i = 0; i < 12; i++) {
            date = date.replace(months[i][0], months[i][1]);
        }
        try {
            DateFormat dateFormat1 = new SimpleDateFormat("dd.MM.HH:mm");
            Calendar c1 = Calendar.getInstance();
            c1.setTimeInMillis(dateFormat1.parse(date).getTime());
            Calendar c2 = Calendar.getInstance();
            c2.setTimeInMillis(System.currentTimeMillis());
            c1.set(Calendar.YEAR, c2.get(Calendar.YEAR));
            return new Timestamp(c1.getTimeInMillis());
        } catch (ParseException e) {
            DateFormat dateFormat2 = new SimpleDateFormat("dd.MM.yy HH:mm");
            return new Timestamp(dateFormat2.parse(date).getTime());
        }
    }

    @Override
    public void parsNews(Article article) throws IOException, NullPointerException {
        Document doc = Jsoup.connect(article.getUrl()).get();
        Elements contentPhoto = doc.select("td.newscol > div:first-child p.vignette img");
        Elements contentVideo = doc.select("td.newscol > div:first-child p > iframe");
        Elements date = doc.select("i:has(a[href^=/allnews/])");
        Elements name = doc.select("h1");
        Element annotation = doc.select("td.newscol > div:first-child > p").first();
        Elements text = doc.select("td.newscol > div:first-child > p");

        article.setName(name.text());
        try {
            article.setAnnotation(annotation.text().trim());
            article.setContent(text.text().replace(article.getAnnotation(), "").trim());
        } catch (NullPointerException e) {
            Document doc1 = Jsoup.parse(doc.select("td.newscol > div:first-child").toString().replace("&nbsp;", " "));
            Elements elements = doc1.select("div > p, p > i");
            article.setAnnotation(elements.text().substring(0, elements.text().indexOf(".") + 1).trim());
            article.setContent(elements.text().replace(article.getAnnotation(), "").trim());
        }
        if (isInteresting(article)) {
            article.setFindingDate(new Timestamp(System.currentTimeMillis()));
            try {
                article.setPublishingDate(parsDate(date.text()));
            } catch (ParseException e) {
                e.printStackTrace();
                article.setPublishingDate(article.getFindingDate());
            }
            article.setSite(site);
            articleDAO.addArticle(article);
            parsVideo(contentVideo, article);
            parsPhoto(contentPhoto, article);

        }
    }
}
