//package com.onapt.project.service.impl;
//
//import com.onapt.project.dao.interf.*;
//import com.onapt.project.entity.*;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.mail.MailException;
//import org.springframework.mail.SimpleMailMessage;
//import org.springframework.mail.javamail.JavaMailSender;
//import org.springframework.scheduling.annotation.EnableScheduling;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//
//import java.sql.Timestamp;
//import java.util.Calendar;
//import java.util.List;
//import java.util.StringTokenizer;
//
//@Component
////@EnableScheduling
//public class AutomaticMailingService {
//
//    @Autowired
//    private MailingDAO mailingDAO;
//
//    @Autowired
//    private ArticleDAO articleDAO;
//
//    @Autowired
//    private SendArticleDAO sendArticleDAO;
//
//    @Autowired
//    private MSiteDAO mSiteDAO;
//
//    @Autowired
//    private MWordDAO mWordDAO;
//
//    @Autowired
//    private WordDAO wordDAO;
//
//    @Autowired
//    private SiteDAO siteDAO;
//
//    @Autowired
//    private MailingListDAO mailingListDAO;
//
//    @Autowired
//    private TIntervalDAO tIntervalDAO;
//
//    @Autowired
//    public JavaMailSender emailSender;
//
//    //      @Scheduled(fixedRate = 600000)
//    public void run() {
////        StringBuilder stringBuilder = new StringBuilder("111").append("222").insert(3,"000");
//
//        List<Mailing> mailings = mailingDAO.getAllMailings();
//
//        for (Mailing mailing : mailings) {
//            if (isStartMailing(mailing)) {
//                sendMail(mailing);
//            }
//        }
//    }
//
//
//    private boolean isStartMailing(Mailing mailing) {
//        List<MailingList> mailingLists = mailingListDAO.getAllMailingListBy(mailing.getMailingId());
//        if (mailingLists.iterator().hasNext()) {
//            MailingList mailingList = mailingLists.iterator().next();
//            Timestamp lastdate = mailingList.getDatatime();
//            Calendar calendar = Calendar.getInstance();
//            calendar.setTimeInMillis(lastdate.getTime());
//            calendar.add(Calendar.DATE, mailing.gettInterval().getInterval());
//            Timestamp nextdate = new Timestamp(calendar.getTime().getTime());
//            if (lastdate.compareTo(nextdate) < 0) {
//                return true;
//            } else {
//                return false;
//            }
//        } else {
//            return true;
//        }
//    }
//
//    private void sendMail(Mailing mailing) {
//        StringBuilder textMessage = new StringBuilder();
//        MailingList mailingList = new MailingList();
//        mailingList.setMailing(mailing);
//        mailingList.setDatatime(new Timestamp(System.currentTimeMillis()));
//        Timestamp dateTime = mailingList.getDatatime();
//        mailingListDAO.addMailingList(mailingList);
//        mailingList = mailingListDAO.getMailingListBy(dateTime);
//        String subjectTemplate = "ОНАХТ парсер - розсилка " + mailingList.getDatatime();
//
//        List<MSite> mSites = mailing.getMSites();
//        for (MSite mSite : mSites) {
//            StringBuilder stringBuilder = new StringBuilder();
//            List<Article> articles;
//            if (mSite.getLastSendArticleId() == null || mSite.getLastSendArticleId() == 0) {
//                articles = articleDAO.getAllArticlesBy(mSite.getSite().getSiteId());
//            } else {
//                articles = articleDAO.getAllArticlesBy(mSite.getSite().getSiteId(), articleDAO.getArticleBy(mSite.getLastSendArticleId()).getPublishingDate());
//            }
//
//            if (articles.iterator().hasNext()) {
//                mSite.setLastSendArticleId(articles.iterator().next().getArticleId());
//                mSiteDAO.updateMSite(mSite);
//                for (Article article : articles) {
//                    if (isInterestingWithMailing(article, mailing)) {
//                        SendArticle sendArticle = new SendArticle();
//                        sendArticle.setArticle(article);
//                        sendArticle.setMailingList(mailingList);
//                        sendArticleDAO.addSendArticle(sendArticle);
//                        stringBuilder.append(article.getName()).append(" \n")
//                                .append(article.getUrl()).append(" \n");
//                    }
//                }
//                if (stringBuilder.toString().isEmpty()) {
//                    textMessage.append(mSite.getSite().getName()).append(" \n").append(" \n")
//                            .append(stringBuilder.toString()).append(" \n");
//                }
//            }
//        }
//        if (textMessage.toString().isEmpty())
//               sendMessage(mailing.getEmail(), subjectTemplate, textMessage.toString());
//    }
//
//    public boolean isInterestingWithMailing(Article article, Mailing mailing) {
//
//        List<MWord> mWords = mailing.getMWords();
//        for (MWord mWord : mWords) {
//            Word word = mWord.getWord();
//
//            StringTokenizer st = new StringTokenizer(word.getWords().replace("\n", ""), ";");
//            while (st.hasMoreTokens()) {
//                String w = st.nextToken();
//                if (article.getName().toLowerCase().contains(w.toLowerCase()) ||
//                        article.getContent().toLowerCase().contains(w.toLowerCase()) ||
//                        article.getAnnotation().toLowerCase().contains(w.toLowerCase())) {
//                    return true;
//                }
//            }
//        }
//        return false;
//    }
//
//    public void sendMessage(String to, String subject, String text) {
//        try {
//            SimpleMailMessage message = new SimpleMailMessage();
//            message.setTo(to);
//            message.setSubject(subject);
//            message.setText(text);
//
//            emailSender.send(message);
//        } catch (MailException exception) {
//            exception.printStackTrace();
//        }
//    }
//}
