package com.onapt.project.service.interf;

import com.onapt.project.entity.Article;

import java.io.IOException;

public interface Parser {
    void initialization(Integer siteId);

    void searchNews();

    void parsNews(Article article) throws IOException;

    boolean isInteresting(Article article);
}
