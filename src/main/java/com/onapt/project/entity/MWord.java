package com.onapt.project.entity;

import javax.persistence.*;

@Entity
@Table(name = "mwords", schema = "public", catalog = "postgres")
public class MWord {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "mw_id")
    private int mwId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "mailing_id")
    private Mailing mailing;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "word_id")
    private Word word;

    public int getMwId() {
        return mwId;
    }

    public Mailing getMailing() {
        return mailing;
    }

    public void setMailing(Mailing mailing) {
        this.mailing = mailing;
    }

    public Word getWord() {
        return word;
    }

    public void setWord(Word word) {
        this.word = word;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MWord mword = (MWord) o;

        if (mwId != mword.mwId) return false;
        if (mailing != null ? !mailing.equals(mword.mailing) : mword.mailing != null) return false;
        if (word != null ? !word.equals(mword.word) : mword.word != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = mwId;
        result = 31 * result + (mailing != null ? mailing.hashCode() : 0);
        result = 31 * result + (word != null ? word.hashCode() : 0);
        return result;
    }

}
