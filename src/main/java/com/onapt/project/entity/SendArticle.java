package com.onapt.project.entity;

import javax.persistence.*;

@Entity
@Table(name = "send_articles", schema = "public", catalog = "postgres")
public class SendArticle {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "sa_id")
    private int saId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ml_id")
    private MailingList mailingList;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "article_id")
    private Article article;

    public int getSaId() {
        return saId;
    }

    public MailingList getMailingList() {
        return mailingList;
    }

    public void setMailingList(MailingList mailingList) {
        this.mailingList = mailingList;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SendArticle that = (SendArticle) o;

        if (saId != that.saId) return false;
        if (article != null ? !article.equals(that.article) : that.article != null) return false;
        if (mailingList != null ? !mailingList.equals(that.mailingList) : that.mailingList != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = saId;
        result = 31 * result + (article != null ? article.hashCode() : 0);
        result = 31 * result + (mailingList != null ? mailingList.hashCode() : 0);
        return result;
    }
}
