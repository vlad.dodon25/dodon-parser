package com.onapt.project.entity;

import javax.persistence.*;

@Entity
@Table(name = "words", schema = "public", catalog = "postgres")
public class Word {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "word_id")
    private int wordId;

    @Basic
    @Column(name = "name")
    private String name;

    @Basic
    @Column(name = "words")
    private String words;

    public Integer getWordId() {
        return wordId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWords() {
        return words;
    }

    public void setWords(String words) {
        this.words = words;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Word word = (Word) o;

        if (wordId != word.wordId) return false;
        if (name != null ? !name.equals(word.name) : word.name != null) return false;
        if (words != null ? !words.equals(word.words) : word.words != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = wordId;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (words != null ? words.hashCode() : 0);
        return result;
    }
}
