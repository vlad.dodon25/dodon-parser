package com.onapt.project.entity;

import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "sites", schema = "public", catalog = "postgres")
public class Site {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "site_id")
    private int siteId;

    @Basic
    @Column(name = "name")
    private String name;

    @Basic
    @Column(name = "url")
    private String url;

    @Basic
    @Column(name = "pagination")
    private String pagination;

    @Basic
    @Column(name = "last_article_url")
    private String lastArticleUrl;



    public int getSiteId() {
        return siteId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPagination() {
        return pagination;
    }

    public void setPagination(String pagination) {
        this.pagination = pagination;
    }

    public String getLastArticleUrl() {
        return lastArticleUrl;
    }

    public void setLastArticleUrl(String lastArticleUrl) {
        this.lastArticleUrl = lastArticleUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Site site = (Site) o;

        if (siteId != site.siteId) return false;
        if (name != null ? !name.equals(site.name) : site.name != null) return false;
        if (url != null ? !url.equals(site.url) : site.url != null) return false;
        if (pagination != null ? !pagination.equals(site.pagination) : site.pagination != null) return false;
        if (lastArticleUrl != null ? !lastArticleUrl.equals(site.lastArticleUrl) : site.lastArticleUrl != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = siteId;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (pagination != null ? pagination.hashCode() : 0);
        result = 31 * result + (lastArticleUrl != null ? lastArticleUrl.hashCode() : 0);
        return result;
    }

}
