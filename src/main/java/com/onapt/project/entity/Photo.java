package com.onapt.project.entity;

import javax.persistence.*;

@Entity
@Table(name = "photos", schema = "public", catalog = "postgres")
public class  Photo {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "photo_id")
    private int photoId;

    @Basic
    @Column(name = "url")
    private String url;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "article_id")
    private Article article;

    public int getPhotoId() {
        return photoId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Photo photo = (Photo) o;

        if (photoId != photo.photoId) return false;
        if (url != null ? !url.equals(photo.url) : photo.url != null) return false;
        if (article != null ? !article.equals(photo.article) : photo.article != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = photoId;
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (article != null ? article.hashCode() : 0);
        return result;
    }
}
