package com.onapt.project.entity;

import javax.persistence.*;

@Entity
@Table(name = "msites", schema = "public", catalog = "postgres")
public class MSite {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "ms_id")
    private int msId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "mailing_id")
    private Mailing mailing;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "site_id")
    private Site site;

    @Basic
    @Column(name = "last_send_article_id")
    private Integer lastSendArticleId;

    public int getMsId() {
        return msId;
    }

    public Mailing getMailing() {
        return mailing;
    }

    public void setMailing(Mailing mailing) {
        this.mailing = mailing;
    }

    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }

    public Integer getLastSendArticleId() {
        return lastSendArticleId;
    }

    public void setLastSendArticleId(Integer lastSendArticleId) {
        this.lastSendArticleId = lastSendArticleId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MSite msite = (MSite) o;

        if (msId != msite.msId) return false;
        if (mailing != null ? !mailing.equals(msite.mailing) : msite.mailing != null) return false;
        if (site != null ? !site.equals(msite.site) : msite.site != null) return false;
        if (lastSendArticleId != null ? !lastSendArticleId.equals(msite.lastSendArticleId) : msite.lastSendArticleId != null) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = msId;
        result = 31 * result + (mailing != null ? mailing.hashCode() : 0);
        result = 31 * result + (site != null ? site.hashCode() : 0);
        result = 31 * result + (lastSendArticleId != null ? lastSendArticleId.hashCode() : 0);
        return result;
    }
}
