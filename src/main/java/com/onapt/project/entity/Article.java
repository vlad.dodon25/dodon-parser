package com.onapt.project.entity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "articles", schema = "public", catalog = "postgres")
public class Article {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "article_id")
	private int articleId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "site_id")
	private Site site;

	@Basic
	@Column(name = "finding_date")
	private Timestamp findingDate;

	@Basic
	@Column(name = "publishing_date")
	private Timestamp publishingDate;

	@Basic
	@Column(name = "url")
	private String url;

	@Basic
	@Column(name = "name")
	private String name;

	@Basic
	@Column(name = "annotation")
	private String annotation;

	@Basic
	@Column(name = "content")
	private String content;

	@Basic
	@Column(name = "rating")
	private Integer rating;

//	@OneToMany(fetch = FetchType.EAGER, mappedBy = "article")
//	private List<Photo> photos;
//
//	@OneToMany(fetch = FetchType.EAGER, mappedBy = "article")
//	private List<Video> videos;

	public int getArticleId() {
		return articleId;
	}

	public Site getSite() {
		return site;
	}

	public void setSite(Site site) {
		this.site = site;
	}

	public Timestamp getFindingDate() {
		return findingDate;
	}

	public void setFindingDate(Timestamp findingDate) {
		this.findingDate = findingDate;
	}

	public Timestamp getPublishingDate() {
		return publishingDate;
	}

	public void setPublishingDate(Timestamp publishingDate) {
		this.publishingDate = publishingDate;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAnnotation() {
		return annotation;
	}

	public void setAnnotation(String annotation) {
		this.annotation = annotation;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Article article = (Article) o;

		if (articleId != article.articleId) return false;
		if (site != null ? !site.equals(article.site) : article.site != null) return false;
		if (findingDate != null ? !findingDate.equals(article.findingDate) : article.findingDate != null) return false;
		if (publishingDate != null ? !publishingDate.equals(article.publishingDate) : article.publishingDate != null)
			return false;
		if (url != null ? !url.equals(article.url) : article.url != null) return false;
		if (name != null ? !name.equals(article.name) : article.name != null) return false;
		if (annotation != null ? !annotation.equals(article.annotation) : article.annotation != null) return false;
		if (content != null ? !content.equals(article.content) : article.content != null) return false;
		if (rating != null ? !rating.equals(article.rating) : article.rating != null) return false;
		return true;
	}

	@Override
	public int hashCode() {
		int result = articleId;
		result = 31 * result + (site != null ? site.hashCode() : 0);
		result = 31 * result + (findingDate != null ? findingDate.hashCode() : 0);
		result = 31 * result + (publishingDate != null ? publishingDate.hashCode() : 0);
		result = 31 * result + (url != null ? url.hashCode() : 0);
		result = 31 * result + (name != null ? name.hashCode() : 0);
		result = 31 * result + (annotation != null ? annotation.hashCode() : 0);
		result = 31 * result + (content != null ? content.hashCode() : 0);
		result = 31 * result + (rating != null ? rating.hashCode() : 0);
		return result;
	}

//	public List<Video> getVideos() {
//		return videos;
//	}
//
//	public List<Photo> getPhotos() {
//		return photos;
//	}

}
