package com.onapt.project.entity;

import javax.persistence.*;

@Entity
@Table(name = "videos", schema = "public", catalog = "postgres")
public class Video {

    @Id
    @Column(name = "video_id")
    private int videoId;

    @Basic
    @Column(name = "url")
    private String url;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "article_id")
    private Article article;

    public int getVideoId() {
        return videoId;
    }

    public void setVideoId(int videoId) {
        this.videoId = videoId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Video video = (Video) o;

        if (videoId != video.videoId) return false;
        if (url != null ? !url.equals(video.url) : video.url != null) return false;
        if (article != null ? !article.equals(video.article) : video.article != null) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = videoId;
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (article != null ? article.hashCode() : 0);
        return result;
    }

}
