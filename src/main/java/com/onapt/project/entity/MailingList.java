package com.onapt.project.entity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "mailing_list", schema = "public", catalog = "postgres")
public class MailingList {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "ml_id")
    private int mlId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "mailing_id")
    private Mailing mailing;

    @Basic
    @Column(name = "datatime")
    private Timestamp datatime;

    public int getMlId() {
        return mlId;
    }

    public Mailing getMailing() {
        return mailing;
    }

    public void setMailing(Mailing mailing) {
        this.mailing = mailing;
    }

    public Timestamp getDatatime() {
        return datatime;
    }

    public void setDatatime(Timestamp datatime) {
        this.datatime = datatime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MailingList that = (MailingList) o;

        if (mlId != that.mlId) return false;
        if (mailing != null ? !mailing.equals(that.mailing) : that.mailing != null) return false;
        if (datatime != null ? !datatime.equals(that.datatime) : that.datatime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = mlId;
        result = 31 * result + (mailing != null ? mailing.hashCode() : 0);
        result = 31 * result + (datatime != null ? datatime.hashCode() : 0);
        return result;
    }
}
