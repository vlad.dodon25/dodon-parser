package com.onapt.project.entity;

import javax.persistence.*;

@Entity
@Table(name = "time_interval", schema = "public", catalog = "postgres")
public class TInterval {

    @Id
    @Column(name = "ti_id")
    private int tiId;

    @Basic
    @Column(name = "name")
    private String name;

    @Basic
    @Column(name = "interval")
    private Integer interval;

    public int getTiId() {
        return tiId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getInterval() {
        return interval;
    }

    public void setInterval(Integer interval) {
        this.interval = interval;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TInterval tInterval = (TInterval) o;

        if (tiId != tInterval.tiId) return false;
        if (name != null ? !name.equals(tInterval.name) : tInterval.name != null) return false;
        if (interval != null ? !interval.equals(tInterval.interval) : tInterval.interval != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = tiId;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (interval != null ? interval.hashCode() : 0);
        return result;
    }
}
