package com.onapt.project.entity;

import javax.persistence.*;
import java.sql.Time;
import java.util.List;

@Entity
@Table(name = "mailing", schema = "public", catalog = "postgres")
public class Mailing {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "mailing_id")
    private Integer mailingId;

    @Basic
    @Column(name = "email")
    private String email;

    @Basic
    @Column(name = "issending")
    private Boolean issending;

    @Basic
    @Column(name = "time")
    private Time time;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ti_id")
    private TInterval tInterval;

//    @OneToMany(fetch = FetchType.EAGER, mappedBy = "mailing")
//    private List<MWord> mWords;
//
//    @OneToMany(fetch = FetchType.EAGER, mappedBy = "mailing")
//    private List<MSite> mSites;

    public Integer getMailingId() {
        return mailingId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getIssending() {
        return issending;
    }

    public void setIssending(Boolean issending) {
        this.issending = issending;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Mailing mailing = (Mailing) o;

        if (mailingId != mailing.mailingId) return false;
        if (email != null ? !email.equals(mailing.email) : mailing.email != null) return false;
        if (issending != null ? !issending.equals(mailing.issending) : mailing.issending != null) return false;
        if (time != null ? !time.equals(mailing.time) : mailing.time != null) return false;
        if (tInterval != null ? !tInterval.equals(mailing.tInterval) : mailing.tInterval != null)
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = mailingId;
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (issending != null ? issending.hashCode() : 0);
        result = 31 * result + (time != null ? time.hashCode() : 0);
        result = 31 * result + (tInterval != null ? tInterval.hashCode() : 0);
        return result;
    }

    public TInterval gettInterval() {
        return tInterval;
    }

    public void settInterval(TInterval tInterval) {
        this.tInterval = tInterval;
    }

//    public List<MSite> getMSites() {
//        return mSites;
//    }
//
//    public List<MWord> getMWords() {
//        return mWords;
//    }
}
