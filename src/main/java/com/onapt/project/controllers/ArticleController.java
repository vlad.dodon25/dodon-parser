package com.onapt.project.controllers;

//@Controller
//public class ArticleController {
//
//	private ArticleService articleService = new ArticleServiceImpl();
//
//
//	@RequestMapping(value = "article/{id}", method = RequestMethod.GET)
//	public ResponseEntity<Article> getArticleById(@PathVariable("id") Integer id) {
//		Article article = articleService.getArticleById(id);
//		return new ResponseEntity<Article>(article, HttpStatus.OK);
//	}
//
//	@RequestMapping(value = "articles", method = RequestMethod.GET)
//	public ResponseEntity<List<Article>> getAllArticles() {
//		List<Article> list = articleService.getAllArticles();
//		return new ResponseEntity<List<Article>>(list, HttpStatus.OK);
//	}
//
//	@RequestMapping("article")
//	public ResponseEntity<Void> addArticle(@RequestBody Article article, UriComponentsBuilder builder) {
//		boolean flag = articleService.addArticle(article);
//		if (flag == false) {
//			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
//		}
//		HttpHeaders headers = new HttpHeaders();
//		headers.setLocation(builder.path("/article/{id}").buildAndExpand(article.getArticleId()).toUri());
//		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
//	}
//
//	@RequestMapping("article")
//	public ResponseEntity<Article> updateArticle(@RequestBody Article article) {
//		articleService.updateArticle(article);
//		return new ResponseEntity<Article>(article, HttpStatus.OK);
//	}
//
//	@RequestMapping("article/{id}")
//	public ResponseEntity<Void> deleteArticle(@PathVariable("id") Integer id) {
//		articleService.deleteArticle(id);
//		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
//	}
//
//}