package com.onapt.project.controllers;

import com.onapt.project.dao.interf.ArticleDAO;
import com.onapt.project.dao.interf.PhotoDAO;
import com.onapt.project.dao.interf.SiteDAO;
import com.onapt.project.dao.interf.VideoDAO;
import com.onapt.project.entity.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;

@Controller
public class MainController {

    @Autowired
    private ArticleDAO articleDAO;

    @Autowired
    private SiteDAO siteDAO;

    @Autowired
    private PhotoDAO photoDAO;

    @Autowired
    private VideoDAO videoDAO;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView home() {
        ModelAndView model = new ModelAndView();
        model.setViewName("index");
        model.addObject("articles",articleDAO.getAllArticles());
        model.addObject("siteDAO",siteDAO);
        model.addObject("photoDAO",photoDAO);
        return model;
    }


    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String showHomePage() {
        return "home";
    }

    @RequestMapping(value = "/settings", method = RequestMethod.GET)
    public String showSettingsPage() {
        return "settings";
    }

    @RequestMapping(value = "/statistics", method = RequestMethod.GET)
    public String showStatisticsPage() {
        return "statistics";
    }

    @RequestMapping(value = "/login")
    public ModelAndView getLogin(@RequestParam(value = "error", required = false) String error,
                                 @RequestParam(value = "logout", required = false) String logout) {
        ModelAndView model = new ModelAndView();
        model.setViewName("login");
        return model;
    }

    @RequestMapping(value = "/news/{id}", method = RequestMethod.GET)
    public ModelAndView getNews(@PathVariable("id") Integer id) {
        ModelAndView model = new ModelAndView();
        model.setViewName("news");
        model.addObject("article", articleDAO.getArticleBy(id));
        model.addObject("photos", videoDAO.getAllVideosBy(id));
        model.addObject("videos", photoDAO.getAllPhotosBy(id));
        return model;
    }

}
